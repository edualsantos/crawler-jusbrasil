Exemplo do uso da API do evernote fazendo Crawler no site do JusBrasil.
==============================================
Este sample é baseado em um sample disponibilizado pelo Evernote.
Houve customização enquanto ao uso do Crawler para pegar informações oriundas do POST submetido no form.

Este código não faz tratamento de exceções para analisar conteúdo de outras páginas que não seguem o mesmo padrão do Jusbrasil.

As tags que estão sendo procuradas são : 
Title, h2 e article.
Estas tags são únicas em cada página do JusBrasil, o que facilita a análise do conteúdo.


Você pode visualizar o resultado no Notebook no Evernote [aqui](https://sandbox.evernote.com/pub/junnior/firstnotebook).

A aplicação está no ar neste [link](http://fllup.com.br:3000).

Tks