var Evernote = require('evernote').Evernote;

var config = require('../config.json');
var callbackUrl = "http://fllup.com.br:3000/oauth_callback";
var Crawler = require("crawler");
var url = require('url');

// home page
exports.index = function(req, res) {
  if(req.session.oauthAccessToken) {
    var token = req.session.oauthAccessToken;
    var client = new Evernote.Client({
      token: token,
      sandbox: config.SANDBOX
    });
    
    var noteStore = client.getNoteStore();
    noteStore.listNotebooks(function(err, notebooks){
      req.session.notebooks = notebooks;
      res.render('index');
    });
  } else {
    res.render('index');
  }
};

// OAuth
exports.oauth = function(req, res) {
  var client = new Evernote.Client({
    consumerKey: config.API_CONSUMER_KEY,
    consumerSecret: config.API_CONSUMER_SECRET,
    sandbox: config.SANDBOX
  });

  client.getRequestToken(callbackUrl, function(error, oauthToken, oauthTokenSecret, results){
    if(error) {
      req.session.error = JSON.stringify(error);
      res.redirect('/');
    }
    else { 
      // store the tokens in the session
      req.session.oauthToken = oauthToken;
      req.session.oauthTokenSecret = oauthTokenSecret;

      // redirect the user to authorize the token
      res.redirect(client.getAuthorizeUrl(oauthToken));
    }
  });

};

// OAuth callback
exports.oauth_callback = function(req, res) {
  var client = new Evernote.Client({
    consumerKey: config.API_CONSUMER_KEY,
    consumerSecret: config.API_CONSUMER_SECRET,
    sandbox: config.SANDBOX
  });

  client.getAccessToken(
    req.session.oauthToken, 
    req.session.oauthTokenSecret, 
    req.param('oauth_verifier'), 
    function(error, oauthAccessToken, oauthAccessTokenSecret, results) {
      if(error) {
        console.log('error');
        console.log(error);
        res.redirect('/');
      } else {
        // store the access token in the session
        req.session.oauthAccessToken = oauthAccessToken;
        req.session.oauthAccessTtokenSecret = oauthAccessTokenSecret;
        req.session.edamShard = results.edam_shard;
        req.session.edamUserId = results.edam_userId;
        req.session.edamExpires = results.edam_expires;
        req.session.edamNoteStoreUrl = results.edam_noteStoreUrl;
        req.session.edamWebApiUrlPrefix = results.edam_webApiUrlPrefix;
        res.redirect('/');
      }
    });
};

// Clear session
exports.clear = function(req, res) {
  req.session.destroy();
  res.redirect('/');
};


exports.makeNote = function(req, res){
  
  var noteTitle, noteBody, noteInnerTitlePage, parentNotebook;

  if( (req.session.oauthAccessToken) && (req.body.URL !=='')) {
    
    var token = req.session.oauthAccessToken;
    
    var crawlerThisURL = req.body.URL;
    //Execute this task to search title and body content tags
    var c = new Crawler({
        maxConnections : 10,
        // This will be called for each crawled page
        callback : function (error, result, $) {
            // $ is Cheerio by default
            //a lean implementation of core jQuery designed specifically for the server
            //Normaly there are only one of this items in each page
            result.innerTitle   = $('title').text();
            result.innerContent = $('article').text();
            result.innerHeader  = $('h2').text();

            //Get pages organized just to show 
            var pages = {
              next : '',
              last : ''
            };

            //Get last and Next pages
            $('.small-paginator').find('a').each(function(index, a){
              if(index==0){
                pages.last = $(a).attr('href');
              }else{
                pages.next = $(a).attr('href');
              }
              //We can do recursively
              // c.queue(toQueueUrl);
            });

            //The Guid : f39e39a3-335d-496a-a747-f4d6ac31a82e is for Notebook Jusbrasil created before. Maybe could be organized by date, assumption etc.
            createNoteAtEvernote(token, pages, result.innerHeader, result.innerContent, '', req, function(err, note){
              // console.log('note : ', note);
                if(!err) res.render('index');
            });

        }
    });
    
    //Simple call to crawler and then register the content to evernote
    c.queue(crawlerThisURL);

  } else {
    res.render('index');
  }

res.render('index');
}


var createNoteAtEvernote = function(token, pages, noteTitle, noteBody, parentNotebook, req, callback){
    //Replace and generate something like Página3Cidade18112014DOSP
    noteTitle = noteTitle.replace(/[`~!@#$%^&*()_|+\-=?;:•'",.<>\{\}\[\]\\\/\s]/gi, '');

    if(noteTitle !=='' && noteBody !=='' && token !=''){

      var client = new Evernote.Client({
        token: token,
        sandbox: config.SANDBOX
      });

      var noteStore = client.getNoteStore();

      var nBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
      nBody += "<!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml2.dtd\">";
      nBody += "<en-note>" + noteBody + "</en-note>";
     
      // Create note object
      var ourNote = new Evernote.Note();
      ourNote.title = noteTitle.trim();
      ourNote.content = nBody.trim();
     
      // parentNotebook is optional; if omitted, default notebook is used
      if (parentNotebook && parentNotebook.guid) {
        ourNote.notebookGuid = parentNotebook.guid;
      }
      // Attempt to create note in Evernote account
      noteStore.createNote(ourNote, function(err, note) {
        callback(err, note);
      });
    }
    

};







